package com.nx.fundamentoskotlin

fun main() {
    val pasajeros: Int = 5
    colectivoPasajeros(pasajeros)
}

fun colectivoPasajeros(pasajeros: Int) {
    println("En el colectivo hay $pasajeros pasajeros")
}

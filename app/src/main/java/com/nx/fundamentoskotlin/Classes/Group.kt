package com.nx.fundamentoskotlin.Classes

enum class Group {
    FAMILY, WORK, FRIEND
}
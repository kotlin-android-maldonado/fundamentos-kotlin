package com.nx.fundamentoskotlin.Classes

import com.nx.fundamentoskotlin.newTopic

fun main() {
    newTopic("Clases")
    val phone = Phone(12345678)
    phone.call()
    phone.showNumber()
    //println(phone.number)

    newTopic("Herencia")
    val smartphone = Smartphone(123123123, true)
    smartphone.call()

    newTopic("Sobre Escritura")
    smartphone.showNumber()
    println("Privado? ${smartphone.isPrivate}")

    newTopic("Data Class")
    val myUser = User(0, "Daso", "Maldonado", Group.FAMILY.ordinal)
    val bro = myUser.copy(id = 1, name = "Ivan")
    val friend = bro.copy(id = 2, group = Group.FRIEND.ordinal)


    println(myUser.component3())
    println(myUser)
    println(bro)
    println(friend)

    newTopic("Funciones de Alcance")
    with(smartphone) {
        println("Privado? $isPrivate")
    }

    /*
    friend.group = Group.WORK.ordinal
    friend.name = "Juan"
    friend.lasName = "Tellez"
     */
    friend.apply {
        group = Group.WORK.ordinal
        name = "Juan"
        lasName = "Tellez"
    }
    println(friend)
}
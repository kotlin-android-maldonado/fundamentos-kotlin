package com.nx.fundamentoskotlin.Classes

data class User(val id: Long, var name: String, var lasName: String, var group: Int)

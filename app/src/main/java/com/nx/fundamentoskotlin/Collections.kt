package com.nx.fundamentoskotlin

import com.nx.fundamentoskotlin.Classes.Group
import com.nx.fundamentoskotlin.Classes.User

fun main(){
    newTopic("Colecciones")
    newTopic("Solo Lectura")
    val frutaLista = listOf("Manzana","Banana","Uva","Limon")
    println(frutaLista.get((frutaLista.indices).random()))
    println("Inde de Uva es ${frutaLista.indexOf("Uva")}")

    newTopic("Mutable list")

    val myUser = User(0, "Daso", "Maldonado", Group.FAMILY.ordinal)
    val bro = myUser.copy(id = 1, name = "Ivan")
    val friend = bro.copy(id = 2, group = Group.FRIEND.ordinal)

    val userList = mutableListOf(myUser, bro)
    println(userList)
    userList.add(friend)
    println(userList)
    userList.removeAt(1)
    userList.remove(bro)
    println(userList)

    val userSelectList = mutableListOf<User>()
    println(userSelectList)
    //userSelectList.addAll(userList)
    userSelectList.add(myUser)
    println(userSelectList)
    userSelectList.set(0,bro)
    println(userSelectList)
    userSelectList.add(myUser)
    userSelectList.add(myUser)
    println(userSelectList)

    newTopic("Map")

    val userMap = mutableMapOf<Int,User>()
    println(userMap)
    userMap.put(myUser.id.toInt(),myUser)
    userMap.put(myUser.id.toInt(),myUser)
    println(userMap)
    userMap.put(friend.id.toInt(),friend)
    println(userMap)
    userMap.remove(2)
    println(userMap)
    println(userMap.isEmpty())
    println(userMap.containsKey(0))
    userMap.put(bro.id.toInt(),bro)
    userMap.put(friend.id.toInt(),friend)
    println(userMap)
    println(userMap.keys)
    println(userMap.values)
}
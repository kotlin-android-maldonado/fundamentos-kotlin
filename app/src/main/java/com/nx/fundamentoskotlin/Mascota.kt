package com.nx.fundamentoskotlin

data class Mascota(
    val nombre: String? = null,
    val cola: Boolean = false,
    val color: String
) {
    open fun saludo() {
        println("Hola soy tu nueva mascota.")
        if(nombre.isNullOrEmpty()){
            println("Puedes ponerme un nombre")
        }
        else{
            println("Mi nombre es $nombre")
        }
    }
}
